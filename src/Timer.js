import React, { Component } from 'react';
import './Timer.css';

export default class Timer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            clock: 0
        };
        this.ticker = this.ticker.bind(this);
    }

    ticker() {
        this.setState({
            clock: new Date() - this.props.start
        });
    }

    componentDidMount() {
        this.timer = setInterval(this.ticker, 1000);
    }

    render() {
        let seconds = Math.round(this.state.clock / 1000);
        return (
            <div id="timer">
                <p>you have been on this site for: </p>
                <span>{seconds}</span>
                <p>seconds.</p>
            </div>
        );
    }
}

